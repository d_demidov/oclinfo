#include <omp.h>

#include <vexcl/devlist.hpp>
#include <vexcl/vector.hpp>
#include <vexcl/sparse/matrix.hpp>
#include <vexcl/profiler.hpp>
#include "oclinfo.h"

#ifdef WIN32
#  include <windows.h>
#  include <delayimp.h>
#endif
//---------------------------------------------------------------------------
bool opencl_available() {
#ifdef WIN32
	__try {
		if (FAILED(__HrLoadAllImportsForDll("OpenCL.dll")))
			return false;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		return false;
	}
#endif
	return true;
}

//---------------------------------------------------------------------------
int STDCALL num_devices() {
	if (opencl_available())
		return vex::backend::device_list(vex::Filter::DoublePrecision).size();
	else
		return 0;
}

//---------------------------------------------------------------------------
void STDCALL device_name(int devnum, char *name, int n) {
    auto dev = vex::backend::device_list(
            vex::Filter::DoublePrecision && vex::Filter::Position(devnum));

    if (dev.size() != 1) throw std::runtime_error("Wrong device num!");

    cl::Platform p(dev[0].getInfo<CL_DEVICE_PLATFORM>());

    std::string pname(p.getInfo<CL_PLATFORM_NAME>().c_str());
    std::string dname(dev[0].getInfo<CL_DEVICE_NAME>().c_str());

    std::ostringstream s;
    s << dname << " (" << pname << ")";

    strncpy(name, s.str().c_str(), n);
}

//---------------------------------------------------------------------------
unsigned long long STDCALL device_memory(int devnum) {
    auto dev = vex::backend::device_list(
            vex::Filter::DoublePrecision && vex::Filter::Position(devnum));

    if (dev.size() != 1) throw std::runtime_error("Wrong device num!");

    return dev[0].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
}

//---------------------------------------------------------------------------
int STDCALL device_cores(int devnum) {
    auto dev = vex::backend::device_list(
            vex::Filter::DoublePrecision && vex::Filter::Position(devnum));

    if (dev.size() != 1) throw std::runtime_error("Wrong device num!");

    return dev[0].getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
}

//---------------------------------------------------------------------------
float STDCALL device_score(int devnum) {
    vex::Context ctx(
            vex::Filter::DoublePrecision && vex::Filter::Position(devnum));

    if (ctx.size() != 1) throw std::runtime_error("Wrong device num!");

    const int n  = 100;
    const int n3 = n * n * n;
    std::vector<int> ptr, col;
    std::vector<double> val;

    ptr.reserve(n3 + 1); ptr.push_back(0);
    col.reserve(n3 * 7);

    for(int k = 0, idx = 0; k < n; ++k) {
        bool kbnd = (k == 0 || k == n - 1);
        for(int j = 0; j < n; ++j) {
            bool jbnd = (j == 0 || j == n - 1);
            for(int i = 0; i < n; ++i, ++idx) {
                bool ibnd = (i == 0 || i == n - 1);
                if (kbnd || jbnd || ibnd) {
                    col.push_back(idx);
                } else {
                    col.push_back(idx - n * n);
                    col.push_back(idx - n);
                    col.push_back(idx - 1);
                    col.push_back(idx);
                    col.push_back(idx + 1);
                    col.push_back(idx + n);
                    col.push_back(idx + n * n);
                }

                ptr.push_back(col.size());
            }
        }
    }

    val.resize(col.size(), 1.0);

    vex::sparse::matrix<double, int> A(ctx, n3, n3, ptr, col, val);
    vex::vector<double> x(ctx, n3), y(ctx, n3);

    x = 0.1;
    y = 0.0;

    y += A * x;

    vex::profiler<> prof(ctx);

    prof.tic_cl("spmv");
    for(int i = 0; i < 10; ++i)
        y += A * x;
    float score = 10 / prof.toc("spmv");

    return score;
}

//---------------------------------------------------------------------------
int cpu_cores() {
    return omp_get_max_threads();
}

//---------------------------------------------------------------------------
float cpu_score() {
    const int n  = 100;
    const int n3 = n * n * n;
    std::vector<int> ptr, col;

    ptr.reserve(n3 + 1); ptr.push_back(0);
    col.reserve(n3 * 7);

    for(int k = 0, idx = 0; k < n; ++k) {
        bool kbnd = (k == 0 || k == n - 1);
        for(int j = 0; j < n; ++j) {
            bool jbnd = (j == 0 || j == n - 1);
            for(int i = 0; i < n; ++i, ++idx) {
                bool ibnd = (i == 0 || i == n - 1);
                if (kbnd || jbnd || ibnd) {
                    col.push_back(idx);
                } else {
                    col.push_back(idx - n * n);
                    col.push_back(idx - n);
                    col.push_back(idx - 1);
                    col.push_back(idx);
                    col.push_back(idx + 1);
                    col.push_back(idx + n);
                    col.push_back(idx + n * n);
                }

                ptr.push_back(col.size());
            }
        }
    }

    std::vector<double> val(col.size(), 1.0);
    std::vector<double> x(n3, 0.1);
    std::vector<double> y(n3, 0.0);

    double tic = omp_get_wtime();
    for(int t = 0; t < 10; ++t) {
#pragma omp parallel for
        for(int i = 0; i < n3; ++i) {
            double s = 0.0;
            for(int j = ptr[i], e = ptr[i+1]; j < e; ++j)
                s += val[j] * x[col[j]];
            y[i] += s;
        }
    }
    double toc = omp_get_wtime();

    return 10 / (toc - tic);
}
