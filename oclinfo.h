#ifndef OCLINFO_H
#define OCLINFO_H

#ifdef WIN32
#  define STDCALL __stdcall
#else
#  define STDCALL
#endif

extern "C" {

// Число устройств, поддерживающих OpenCL
int STDCALL num_devices();

// Для устройства с номером devnum (0 <= devnum < num_devices())
// возвращает в строке name имя устройства (не более n символов).
void STDCALL device_name(int devnum, char *name, int n);

// Для устройства с номером devnum (0 <= devnum < num_devices())
// возвращает объем памяти в байтах.
unsigned long long STDCALL device_memory(int devnum);

// Для устройства с номером devnum (0 <= devnum < num_devices())
// возвращает число "вычислительных единиц" (compute units).
// Для CPU это ядра, для видеокарт -- мультипроцессоры.
int STDCALL device_cores(int devnum);

// Для устройства с номером devnum (0 <= devnum < num_devices())
// возвращает оценку его производительности --
// число умножений разреженной матрицы на вектор в секунду.
// Чем выше оценка, тем быстрее устройство.
// Структура тестовой матрицы соответствует трехмерной задаче Пуассона,
// дискретизированной методом конечных разностей на сетке 100x100x100.
float STDCALL device_score(int devnum);

// Число ядер центрального процессора.
int cpu_cores();

// Оценка производительности для центрального процессора --
// число умножений разреженной матрицы на вектор в секунду.
float cpu_score();

}

#endif
